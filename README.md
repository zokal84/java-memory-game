# Java Memory Game

Basic Java memory game project that uses JFrames, JOptionPane, Images, JToggleButtons, and such. I created this for a school project a few years back. The hard mode part of the project is incomplete, but it can still be viewable/used. It just does not keep score or anything like that.

The main class for the project is named "ZokalFinalProject.class"